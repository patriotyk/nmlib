# -*- coding: utf-8 -*-



class ApType():
    ONE_ROOM = 0
    TWO_ROOMS = 1
    THREE_ROOMS = 2
    FOUR_ROOMS = 3
    HOUSE = 4
    ROOM_PART = 5

class Category():
    RENT = 0
    SALE = 1
    RENT_SLUG = 'orenda'
    SALE_SLUG = 'prodazh'


class Region():
    SHEVCH = 0
    LYCH = 1
    SYKH = 2
    FRANK = 3
    ZALIZ = 4
    HAL = 5

class Material():
    BRICK = 0
    PANEL = 1
    MIX = 2
    WOOD = 3


class Currency():
    USD = 0
    UAH = 1
    EUR = 2
