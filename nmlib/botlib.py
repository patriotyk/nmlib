import json, time
import urllib, urllib2

import sqlite3
import os
import hashlib
import requests

import bz2
import datetime
import time
import traceback

APIURL = 'https://nomakler.com.ua/'
#APIURL = 'http://127.0.0.1:8000/'
USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36'

BOT_STATUS_OK = "Stopped"
BOT_STATUS_FAILED = "Failed"




class Api():
    sessionid = None

    def isMakler(self, phones, name):
        data = []
        for phone in phones:
            data.append(('phones', phone))
        if name:
            data.append(('name', name.encode('utf-8')))

        data = urllib.urlencode(data)

        result = self._request(APIURL + 'api/isMakler?' + data).read()
        result = json.loads(result)
        return result


    def saveMaklerPhones(self, phones):
        data = []
        for phone in phones:
            data.append(('phones', phone))
        result = self._request(APIURL + 'api/addMaklerContacts', urllib.urlencode(data)).read()
        print result
        result = json.loads(result)
        return result

    def botStarted(self):
        return self._request(APIURL + 'api/botStarted')

    def removeOld(self):
        return self._request(APIURL + 'api/removeOld')

    def updateRates(self):
        return self._request(APIURL + 'api/v1/updateRates')

    def getUserItems(self, username, page=1):
        result = self._request(APIURL + 'api/v1/searchitems?username=%s&fields=id,url&page=%d'%(username,page)).read()
        result = json.loads(result)
        return result

    def removeitem(self, id):
        return self._request(APIURL + 'deactivate/%d/'%id)

    def botStoped(self, status, remove=False):
        url = APIURL + 'api/botFinished?status=%s' % status
        if remove:
            url += '&remove_old'
        return self._request(url)

    def upload_img(self, img_url):
        try:
            req = urllib2.Request(img_url, headers={ 'User-Agent': USER_AGENT })
            img_file = urllib2.urlopen(req)
            content_type = img_file.info().gettype();
            response = requests.post(APIURL + "api/v1/upload",
                                     files={'original': (hashlib.sha256(img_url).hexdigest()+"."+content_type[6:], img_file)},
                                     cookies={'sessionid': self.sessionid})
        except:
            print traceback.format_exc()
        return json.loads(response.content)


    def addAps(self, aps):
      response = requests.post(APIURL + "api/addApartments",
                                 files={'data': ('aps.bz2', bz2.compress(json.dumps(aps)))},
                                 cookies={'sessionid': self.sessionid})

    def addAp(self, ap):
        if ap.get('photos'):
            p_urls = ap.pop('photos')
            ids = []
            for url in p_urls:
                if url.strip():
                    try:
                      result = self.upload_img(url)
                      ids.append(result[0]["id"])
                    except:
                      print traceback.format_exc()

            if len(ids):
                ap['photos'] = ids
        result = self._request(APIURL + 'api/addApartment', json.dumps(ap)).read()
        result = json.loads(result)
        return result

    def _request(self, url, data=None):
        req = urllib2.Request(url)
        response = None
        if self.sessionid:
            req.add_header("Cookie", "sessionid=" + self.sessionid)
        i = 3
        while i:
            try:
                response = urllib2.urlopen(req, data)
                break
            except urllib2.HTTPError as e:
                print "url - %s, failed waitng 3 secs." % url
                i -= 1
                time.sleep(2)
                if i == 0:
                    error_message = e.read()
                    print error_message
        return response

    def login(self, username, password):
        data = []
        data.append(('username', username))
        data.append(('password', password))
        data = urllib.urlencode(data)

        responce = self._request(APIURL + 'api/auth', data)
        body = responce.read()
        print body
        result = json.loads(body)

        if responce.code == 200 and result['status'] == 'ok':
            self.sessionid = result['sessionid']
            return True
        return False


cacheShema = """
CREATE TABLE IF NOT EXISTS `cache` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  `hash` varchar(64) NOT NULL,
  `type` varchar(128) NOT NULL,
  `timestamp` DATE DEFAULT (datetime('now','localtime'))

)"""

removeOld = "delete from cache where type='ad' and timestamp<datetime(%s, 'unixepoch');"% str(time.time()-datetime.timedelta(days=5).total_seconds())

class Cache():

    def __init__(self, name):
        self.db_path = os.path.expanduser('~/.nomakler')
        if not os.path.exists(self.db_path):
            os.makedirs(self.db_path)
        filename = os.path.join(self.db_path, '%s-cache.db' % name)
        print "FILENAME: %s" % filename
        self.conn = sqlite3.connect(filename)
        cur = self.conn.cursor()
        cur.execute(cacheShema)
        cur.execute(removeOld)
        self.conn.commit()

    def in_cache(self, url, type):
        cur = self.conn.cursor()
        hash = hashlib.sha256(url).hexdigest()
        cur.execute(u"SELECT * FROM cache WHERE hash = '%s' AND type='%s'" % (hash, type))
        if cur.fetchone():
            return True
        else:
            return hash


    def add(self, hash, type):
        cur = self.conn.cursor()
        cur.execute(u"INSERT INTO cache (hash, type) values(?,?)", (hash, type))
        self.conn.commit()
