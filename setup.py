#!/usr/bin/env python

import os
from setuptools import setup, find_packages

setup(name='nmlib',
      version='1.2',
      description='nomakler api project',
      author='Serhiy Stetskovych',
      author_email='patriotyk@gmail.com',
      url='https://nomakler.com.ua/',
      packages=find_packages(),
)

